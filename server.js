const PORT = 8900
var http = require('http')
var https = require('https')
var fs = require('fs')
var path = require('path')
var pug = require('pug')
var mime = require('mime-types')
var stylus = require('stylus')

// for electron app
const electron = require('electron')
const Menu = electron.Menu
const app = electron.app
const BrowserWindow = electron.BrowserWindow
const electronpug = require('electron-pug')({pretty: true})

const url = require('url')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

// Assign prototype directory and create it if it does not exist
var PROTOTYPE_DIRECTORY = process.env['HOME'] + '/Documents/Prototypes'
if(!fs.existsSync(PROTOTYPE_DIRECTORY)) fs.mkdirSync(PROTOTYPE_DIRECTORY)

var proto = {
  version: "1.0.3",
  server: http.createServer(function (req, res) {
    req.url = req.url.replace("%20", " ")
    fs.stat(PROTOTYPE_DIRECTORY + req.url, function(err, stats){
      if(err) {
        if(err.message.indexOf('no such file or directory') != -1){
          res.writeHead(404, { 'Content-Type' : 'text/html' })
          res.end("File not found: " + req.url, 'utf-8')
        }
        else {
          proto.handler.error(req, res, err)
        }
        return 0
      }
      if(stats.isDirectory()) {
        proto.handler.folder(req, res)
      }
      else if(stats.isFile()){
        var ext = path.extname(req.url)
        switch(ext){
          case '.pug':
            proto.handler.pug(req,res)
            break
          case '.styl':
            proto.handler.styl(req,res)
            break
          default:
            proto.handler.file(req, res)
            break
        }
      }
      else {
        page.log('{error}MYSTERY FILE! Spooky')
      }
    })
  }),
  handler: {
    error: function(req, res, err){
      res.writeHead(500, { 'Content-Type' : 'text/html' })
      res.end('<pre>' + err.message + '</pre>', 'utf-8')
      var msg = err.message.replace(PROTOTYPE_DIRECTORY + '/', '')

      page.log("{error}" + msg)
    },
    pug: function(req, res){
      var filePath = PROTOTYPE_DIRECTORY + req.url
      var options = {
        pretty: true,
        filters: {
  				stylus: function(text){
            //if we're just including a separate file, make sure we set the stylusPath variable so stylus knows context
            if(text.match(/^include/)){
              var stylusImport = text.trim().split(' ')
              var folder = req.url.split('/')
              folder.pop()
              stylusPath = folder.join('/') + '/' + stylusImport[1]
              page.log('{warn}stylus import detected: <div class="url">' + stylusPath + "</div>")
              stylusPath = PROTOTYPE_DIRECTORY + stylusPath
              var output = ''
              var css = fs.readFileSync(stylusPath, 'utf8')
              stylus.render(css, {filename: stylusPath}, (err, css) => {
                if(err){
                  page.log('{error}stylus compilation failed: <div class="url">' + req.url + "</div>")
                  output = err.message
                }
                else {
                  page.log('{success}compiled stylus inline: <div class="url">' + req.url + "</div>")
                  output = css
                  return output
                }
              })
              output = '\n<style type="text/css">\n' + output + '</style>'
              return(output)
            }
            //otherwise just interpret the text block
            stylus.render(text, {filename: filePath}, (err, css) => {
              if(err){
                output = err.message
                page.log('{error}pug compilation failed')
                page.log('{error}' + err.message)
                return (err.message)
              }
              page.log('{success}stylus inline block compiled')
              output = '<style type="text/css">' + css + '</style>'
              return css
            })
            return output
  				},
          code: function(text, options){
            var entityMap = {
              "&": "&amp;",
              "<": "&lt;",
              ">": "&gt;",
              '"': '&quot;',
              "'": '&#39;',
              "/": '&#x2F;'
            }

            text = text.replace(/[&<>"'\/]/g, function (s) {
              return entityMap[s];
            })

            var preClass = Object.keys(options).length > 1 ? Object.keys(options)[1] : ''
            text = "<pre class='" + preClass + "'><code class='language-" + Object.keys(options)[0] + "'>" + text + "</code></pre>"

            return text
          }

        }
      }
      pug.renderFile(filePath, options, function(err, html){
        if(err) proto.handler.error(req, res, err)
        res.writeHead(200, { 'Content-Type' : 'text/html' })
        res.end(html, 'utf-8')
      })
    },
    styl: function(req, res){
      var filePath = PROTOTYPE_DIRECTORY + req.url
      fs.readFile(filePath, 'utf8', (err, data) => {
        if(err){
          proto.handler.error(req, res, err)
        }
        else {
          stylus.render(data, {filename: filePath}, (err, css) => {
            if(err){
              page.log('{error}stylus compilation failed: <div class="url">' + req.url + "</div>")
              proto.handler.error(req, res, err)
            }
            else {
              page.log('{success}compiled stylus file: <div class="url">' + req.url + "</div>")
              res.writeHead(200, { 'Content-Type' : 'text/css' })
              res.end(css, 'utf-8')
            }
          })
        }
      })
    },
    folder: function(req, res){
      var html = 'no index found'
      var url = req.url
      if(!url.match(/\/$/)){
        url += '/'
        res.writeHead(302, {
          'Location': url
        })
        res.end()
        return 0
      }
      var filePath = PROTOTYPE_DIRECTORY + url
      fs.readdir(filePath, function(err, files){
        res.writeHead(200, { 'Content-Type' : 'text/html' })
        if(err) {
          page.log('{error}error rendering directory')
          page.log('{error}' + err)
          res.end(err.toString(), 'utf-8')
          return 0
        }
        if(files.indexOf('index.pug') != -1){
          req.url += 'index.pug'
          proto.handler.pug(req, res)
        }
        else {
          html = '<h1>Directory Listing</h1><ul>'
          for(var i = 0; i < files.length; i++){
            var path = url + files[i]
            html += '<li><a href="' + path + '">' + files[i] + '</a></li>'
          }
        }
        res.end(html, 'utf-8')
      })
    },
    file: function(req, res){
      var mimetype = mime.lookup(req.url)
      if(mimetype == 'video/mp4' && req.headers.range){
        var file = PROTOTYPE_DIRECTORY + req.url
        var range = req.headers.range
        var positions = range.replace(/bytes=/, "").split("-")
        var start = parseInt(positions[0], 10)

        fs.stat(file, function(err, stats) {
          var total = stats.size
          var end = positions[1] ? parseInt(positions[1], 10) : total - 1
          var chunksize = (end - start) + 1

          res.writeHead(206, {
            "Content-Range": "bytes " + start + "-" + end + "/" + total,
            "Accept-Ranges": "bytes",
            "Content-Length": chunksize,
            "Content-Type": "video/mp4"
          });

          var stream = fs.createReadStream(file, { start: start, end: end })
            .on("open", function() {
              stream.pipe(res)
            }).on("error", function(err) {
              res.end(err)
            })
        })
      }
      else {
        fs.readFile(PROTOTYPE_DIRECTORY + req.url, function(err, data){
          if(err){
            page.log('{error}File not found: <div class="url">' + req.url + '</div>')
            res.writeHead(404,{"Content-type":"text/plain"})
            res.end("Sorry the page was not found")
          }
          else {
            if(mimetype)
              res.writeHead(200, { 'Content-Type': mimetype })
            res.end(data)
          }
        })
      }
    }
  }
}

/*
 * Electron functions below
*/
var page = {
  createWindow: function() {
    // Create new window
    mainWindow = new BrowserWindow({width: 800, height: 600, titleBarStyle: 'hidden'})

    // Load index.pug into the new window
    mainWindow.loadURL(url.format({
      pathname: path.join(__dirname, 'index.pug'),
      protocol: 'file:',
      slashes: true
    }))

    // Open the DevTools.
    // mainWindow.webContents.openDevTools()

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
      // Dereference the window object, usually you would store windows
      // in an array if your app supports multi windows, this is the time
      // when you should delete the corresponding element.
      mainWindow = null
    })

    mainWindow.webContents.on('did-finish-load', () => {
      // A call is made to check whether updates to the proto server are available
      page.checkForUpdates()
      // The proto server gets started here once the mainwindow has finished loading
      proto.server.listen(PORT)
      page.log('{success} listening on <div class="url" style="display: inline">' + PORT + "</div>")
    })
  },
  checkForUpdates: function() {
    https.get('https://syd.jjcm.org/server/version', (res) => {
      const statusCode = res.statusCode
      const contentType = res.headers['content-type']

      let error
      if (statusCode !== 200) {
        error = new Error(`Request Failed.\n` +
                          `Status Code: ${statusCode}`)
        console.log(error.message)
        // consume response data to free up memory
        res.resume()
        return
      }

      res.setEncoding('utf8')
      let rawData = ''
      res.on('data', (chunk) => rawData += chunk)
      res.on('end', () => {
        try {
          let parsedData = String(rawData).replace(/^\s+|\s+$/gm,'')
          if (parsedData != proto.version) {
            page.updateRequired()
          }
          console.log("Most up to date version is: " + parsedData)
          console.log("Current version is: " + proto.version)
        } catch (e) {
          console.log(e.message)
        }
      });
    }).on('error', (e) => {
      console.log(`Got error: ${e.message}`)
    });
  },
  log: function(data) {
    console.log(data)
    mainWindow.webContents.send('log', data)
  },
  sendProtoDirectory: function(data){
    mainWindow.webContents.send('directory', data)
  },
  updateRequired: function() {
    mainWindow.webContents.send('update')
  }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.on('ready', page.createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    page.createWindow()
  }
})
