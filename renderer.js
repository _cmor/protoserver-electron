const {app, remote, ipcRenderer, shell} = require('electron')
const {Menu, MenuItem, BrowserWindow} = remote
var fs = require('fs')
var utils = require('./utils.js')
var path = require('path')
var Sftp = require('sftp-upload')
const fixPath = require('fix-path')

const chokidar = require('chokidar')
const exec = require('child_process').exec
const Git = require('nodegit')
const GitUrlParse = require('git-url-parse')
const LOCAL_URL = 'localhost:8900'
const PROTOTYPE_DIRECTORY = process.env['HOME'] + '/Documents/Prototypes'

var renderer = {
  logContainer: document.getElementById("log"),
  directories: [],
  clickAway: false,
  editors: ['atom', 'code', 'sublime'],
  selected: null,
  watcher: null,
  generalWatcher: null,
  preview: null,
  rightClick: null,
  numUploads: 0,
  // create window for live preview of prototypes
  init: function() {
    // set the path appropriately for the app for commands executed later
    fixPath();

    // Make renderer available in the dev tools
    window.renderer = renderer

    // Listen to messages from server on log channel
    ipcRenderer.on("log", (event, message) => {
      renderer.log(message)
    })

    window.addEventListener("beforeunload", function() {
      if (renderer.preview) {
        renderer.preview.close()
      }
    })

    fs.readdir(PROTOTYPE_DIRECTORY, function(err, files){
      if(err) {
        page.log('error rendering prototype directory')
        page.log(err)
        return 0
      }
      else {
        files = files.filter(function(file){
          return fs.statSync(path.join(PROTOTYPE_DIRECTORY, file)).isDirectory()
        })
        renderer.directories = files
        renderer.directories.sort(function(a, b) {return a.localeCompare(b, 'en', { 'sensitivity': 'base'})}).map(function(dir){
          renderer.createDirLi(dir)
        })
      }
    })

    // Commented out as account settings has been temporarily removed
    // document.getElementById("settings").addEventListener("click", function () {
    //   document.getElementById("settingsPage").classList.toggle("current")
    //   document.getElementById("logPage").classList.toggle("current")
    // })

    document.getElementById("create").addEventListener("click", renderer.createNewProject)
    renderer.setIconToShowInvalidProjectMessage(document.getElementById("newProject").querySelector(".inlineIcon"))

    // add listeners to determine project name finalised
    document.getElementById("newProjectName").addEventListener("keypress", function(e) {
      if (e.key == "Enter") {
        document.getElementById("newProjectName").blur()
        if (!document.getElementById("newProjectName").value.includes(" ")) {
          renderer.initiateNew()
        }
      } else if (e.key == " ") {
        document.getElementById("newProject").querySelector(".directoryItem").classList.add("warn")
      } else if (!document.getElementById("newProjectName").value.includes(" ")) {
        document.getElementById("newProject").querySelector(".directoryItem").classList.remove("warn")
      }
    })

    // add listener to check if warn can be removed after backspace
    document.getElementById("newProjectName").addEventListener("keyup", function(e) {
      if (e.key == "Backspace" && !document.getElementById("newProjectName").value.includes(" ")) {
        document.getElementById("newProject").querySelector(".directoryItem").classList.remove("warn")
      }
    })

    // Listen to messages from server on update channel
    ipcRenderer.on("update", (event, message) => {
      renderer.toggleFlag(document.getElementById("updateFlag"))
    })

    // Watch if a new directory has been added or deleted and refresh if so
    var ignoredExp = new RegExp(PROTOTYPE_DIRECTORY + "\/.*\/.*")
    renderer.watcher = chokidar.watch(PROTOTYPE_DIRECTORY, {ignored: ignoredExp, ignoreInitial: true}).on('all', (event, path) => {
      var dir = path.replace(PROTOTYPE_DIRECTORY + "/", "")
      if (event == 'addDir') {
        renderer.makeNewDirLi(dir)
      } else if (event == 'unlinkDir') {
        renderer.deleteDirLi(path.replace(PROTOTYPE_DIRECTORY + "/", ""))
      }
    })

    // Create right click button

    const menu = new Menu()
    let menuItem = new MenuItem({
      label: 'Delete',
      click: () => {
        renderer.tryDeleteProject(prototype)
      }
    })
    let menuItem1 = new MenuItem({
      label: 'Rename',
      click: () => {
        renderer.addRenameField(document.querySelector("#" + prototype + " div.name"))
      }
    })
    let menuItem2 = new MenuItem({
      label: 'Duplicate',
      click: () => {
        var matches = prototype.match(/\d+$/);
        if (matches) {
          number = String(parseInt(matches[0]) + 1)
          copy = prototype.replace(/\d+$/, number)
        } else {
          copy = prototype + "2"
        }

        renderer.duplicateProject(PROTOTYPE_DIRECTORY + "/" + prototype, PROTOTYPE_DIRECTORY + "/" + copy)
      }
    })
    menu.append(menuItem)
    menu.append(menuItem1)
    menu.append(menuItem2)
    window.addEventListener('contextmenu', (e) => {
      e.preventDefault()
      let dir = utils.classInLineage("directoryItem", document.elementFromPoint(e.x, e.y))
      if (dir && dir.id != "newProjectLi") {
        prototype = dir.id
        menu.popup(remote.getCurrentWindow())
      }
    }, false)

    // set upload button, this hooks into whatever renderer.selected currently is'
    // Commented out as uploading functionality has momentarily been removed
    // renderer.setUpload()
  },
  /* * * * * * * * * * *
  *    NAVIGATION      *
  * * * * * * * * * * */
  resetSelected: function() {
    renderer.watcher.close()
    renderer.watcher = null
    document.getElementById(renderer.selected).classList.remove('selected')
    renderer.selected = null

    // Commented out as uploading functionality has momentarily been removed
    // renderer.showUpload()
  },
  openDirectory: function(dir) {
    //renderer.log("trying to open " + dir + " in a text editor <br>")
    renderer.tryOpen(dir, 0)
  },
  // tries to open dir with the editor at index i of renderer.editors
  // otherwise makes a call to try to open dir with the next editor
  // if it is the last editor opens the dir in finder
  tryOpen: function(dir, i) {
    if (i < renderer.editors.length) {
      exec(renderer.editors[i] + ' "' + PROTOTYPE_DIRECTORY + '/' + dir + '"', (error, stdout, stderr) => {
        if (error) {
          i++
          renderer.log("{error}" + error + "<br>")
          renderer.tryOpen(dir, i)
        } else {
          renderer.log("{success}opened folder in " + renderer.editors[i] + " <br>")
        }
      })
    } else {
      exec("open " + ' "' + PROTOTYPE_DIRECTORY + '/' + dir + '"', (error) => {
        renderer.log("{warn}no editor found <br>")
      })
    }
  },
  viewDirectory: function(dir) {
    renderer.changeListener(dir)
    if(renderer.selected) document.getElementById('directory').querySelector('.selected').classList.remove('selected')
    renderer.selected = dir
    document.getElementById(dir).classList.add('selected')
    if (!renderer.preview){
      renderer.preview = new BrowserWindow({width: 1200, height: 809, webPreferences: {preload: path.join(__dirname, "preview.js")}})
      renderer.preview.on('closed', () => {
        renderer.preview = null
        renderer.resetSelected()
      })
    }
    renderer.preview.loadURL("http://localhost:8900/" + dir)
  },
  changeListener: function(dir){
    //renderer.log("changing listener " + dir + "<br>")
    if (renderer.selected) {
      //renderer.log("unwatching " + PROTOTYPE_DIRECTORY + "/" + renderer.selected)
      renderer.watcher.close()
    }
    renderer.watcher = chokidar.watch(PROTOTYPE_DIRECTORY + "/" + dir, {ignored: /(^|[\/\\])\../}).on('all', (event, path) => {
      renderer.onChange(event, path)
    })
  },
  onChange: function(event, path){
    if(event != 'add' && event != 'addDir'){
      renderer.log('{warn}change detected: <div class="url">' + path.replace(PROTOTYPE_DIRECTORY + '/', '') + "</div>")
      if (renderer.preview) {
        renderer.preview.reload()
      }
    }
  },
  /* * * * * * * * * * *
  * CONTENT FUNCTIONS  *
  * * * * * * * * * * */
  log: function(data) {
    var logEntry = document.createElement('div')
    logEntry.className = 'log-entry'

    var typeRegex = /(^{.*})(.*)/g
    var type = typeRegex.exec(data)
    if(type){
      switch(type[1]) {
        case "{error}":
          logEntry.classList.add('error')
          break
        case "{success}":
          logEntry.classList.add('success')
          break
        case "{warn}":
          logEntry.classList.add('warn')
          break
        case "{wait}":
          logEntry.classList.add('wait')
          break
        default:
          console.log('default')
          break
      }

      var lineErrorRegex = /(.*pug|.*styl):([0-9]*):([0-9]*)$/
      var lineErrorMatch = lineErrorRegex.exec(type[2])
      if(lineErrorMatch){
        logEntry.innerHTML = lineErrorMatch[1] + "<div class='url'>line: " + lineErrorMatch[2] + "</div>"
      }
      else {
        logEntry.innerHTML = type[2]
      }

      renderer.logContainer.appendChild(logEntry)
      renderer.logContainer.scrollTop = renderer.logContainer.scrollHeight
    }
    else {
      renderer.logContainer.innerHTML += String(data)
      renderer.logContainer.scrollTop = renderer.logContainer.scrollHeight
    }

    return logEntry
  },
  /* * * * * * * * * * * * * * *
  * CREATION, DELETION, RENAME *
  * * * * * * * * * * * * * * */
  createNewProject: function() {
    document.getElementById("directory").classList.toggle("down")
    document.getElementById("newProject").classList.toggle("revealed")
    document.getElementById("newProjectName").focus()
  },
  initiateNew: function() {
    var dir = document.getElementById('newProjectName').value
    var parsedUrl = GitUrlParse(dir)
    if (parsedUrl.protocol) {
      renderer.makeFromGit(parsedUrl)
    } else {
      if (dir === '') {
        var i = 1
        while (renderer.directories.indexOf('proto' + String(i)) !== -1) {
          i++
        }
        dir = 'proto' + String(i)
      }
      renderer.makeTemplate(dir)
    }
    renderer.updateDirList()
  },
  makeFromGit: function(parsedUrl) { // refactoring required
    var repoDir = path.join(PROTOTYPE_DIRECTORY, parsedUrl.name)
    var repoUrl = parsedUrl.href
    var msg = `cloning Git repository from ${repoUrl}`
    var logEntry = renderer.log('{wait}' + `${msg} <b>(0%)</b>`)

    var cloneOpts = {fetchOpts: {callbacks: {}}}
    var callbacks = cloneOpts.fetchOpts.callbacks
    callbacks.transferProgress = function(stats) {
      var progress = (stats.receivedObjects() + stats.indexedObjects()) / (stats.totalObjects() * 2)
      progress = Math.round(100 * progress)
      logEntry.innerHTML = `${msg} <b>(${progress}%)</b>`
    }

    // macOS: workaround for known libgit2 issue with verifying GitHub certificates
    if (process.platform === 'darwin') {
      callbacks.certificateCheck = () => 1
    }

    cloneSuccess = function() {
      logEntry.innerHTML = msg
      logEntry.classList.toggle('wait')
      logEntry.classList.toggle('success')
      renderer.log(`{success}created new project at ${repoDir}`)
    }

    cloneFailure = function(err) {
      logEntry.classList.toggle('wait')
      logEntry.classList.toggle('error')
      renderer.log('{error}' + err.message)
    }

    switch (parsedUrl.protocol) {
      case 'ssh':
        logEntry.classList.toggle('wait')
        logEntry.classList.toggle('error')
        renderer.toggleFlag(document.getElementById('authenticateSshFlag'))
        document.getElementById('gitSshSubmit').addEventListener('click', function() {
          renderer.toggleFlag(document.getElementById('authenticateSshFlag'))
          publicKey = document.getElementById('gitSshPublicKey').value
          privateKey = document.getElementById('gitSshPrivateKey').value
          password = document.getElementById('gitSshPassword').value
          callbacks.credentials = function(url, username) {
            return Git.Cred.sshKeyFromAgent(username, publicKey, privateKey, password)
          }
          logEntry.classList.toggle('error')
          logEntry.classList.toggle('wait')
          Git.Clone(repoUrl, repoDir, cloneOpts)
            .then(cloneSuccess)
            .catch(cloneFailure)
        })
        break
      case 'http':
      case 'https':
        Git.Clone(repoUrl, repoDir, cloneOpts)
          .then(cloneSuccess)
          .catch(function(err) {
            if (err.message !== 'authentication required but no callback set') {
              cloneFailure(err)
            } else {
              logEntry.classList.toggle('wait')
              logEntry.classList.toggle('error')
              renderer.toggleFlag(document.getElementById('authenticateHttpFlag'))
              document.getElementById('gitHttpSubmit').addEventListener('click', function() {
                renderer.toggleFlag(document.getElementById('authenticateHttpFlag'))
                username = document.getElementById('gitHttpUsername').value
                password = document.getElementById('gitHttpPassword').value
                callbacks.credentials = function() {
                  return Git.Cred.userpassPlaintextNew(username, password)
                }
                logEntry.classList.toggle('error')
                logEntry.classList.toggle('wait')
                Git.Clone(repoUrl, repoDir, cloneOpts)
                  .then(cloneSuccess)
                  .catch(cloneFailure)
              })
            }
          })
        break
      default:
        renderer.log('{error}unsupported protocol type for Git URL')
        break
    }
  },
  tryDeleteProject: function(dir){
    let flag = document.getElementById("deleteFlag")
    flag.querySelector(".flagMessage").innerHTML = "Are you sure you want to delete \"" + dir + "\"?"
    flag.querySelector(".flagMessage").innerHTML += "<img src='https://media.giphy.com/media/L9j3uTtWcV32/giphy.gif' />"
    document.getElementById("deleteProto").onclick = function () {
      renderer.toggleFlag(flag)
      renderer.deleteProject(dir)
    }
    document.getElementById("keepProto").onclick = function () {
      renderer.toggleFlag(flag)
    }
    renderer.toggleFlag(flag)
  },
  deleteProject: function(dir){
    exec('rm -r ' + PROTOTYPE_DIRECTORY + '/' + dir, (error) => {
      if (error) {
        renderer.log("{error}failed to delete prototype " + dir)
        renderer.log("{error}" + error)
      } else {
        renderer.log("{success}deleted prototype " + dir)
      }
    })
  },
  makeTemplate: function(dir) {
    renderer.duplicateProject(__dirname + "/template", PROTOTYPE_DIRECTORY + "/" + dir)
  },
  duplicateProject: function(original, copy) {
    fs.stat(copy, function (error) {
      if (error) {
        //renderer.log("directory " + dir + " does not exist yet")
        exec('cp -r "' + original + '" "' + copy + '"', (error) => {
          if (error) {
            renderer.log("{error}failed to make new directory " + copy + " from " + original)
            renderer.log("{error}" + error)
          } else {
            renderer.log("{success}made new directory " + copy + " from " + original)
          }
        })
      } else {
        renderer.log("{error}directory " + copy + " already exists")
      }
    })
  },
  updateDirList: function() {
    document.getElementById("newProject").classList.toggle("revealed")
    document.getElementById("directory").classList.toggle("down")
    document.getElementById("newProjectName").value = ""
    // do not need to specifically add the new directory to the list as this should be dealt with
    // by the listener
  },
  makeNewDirLi: function(dir) {
    renderer.directories.push(dir)
    renderer.directories.sort(function(a, b) {
      return a.localeCompare(b, 'en', { 'sensitivity': 'base'})
    })
    renderer.createDirLi(dir, renderer.directories.indexOf(dir))
  },
  deleteDirLi: function(dir) {
    renderer.directories.splice(renderer.directories.indexOf(dir), 1)
    document.getElementById("directory").removeChild(document.getElementById(dir))
  },
  doubleClickLi: false,
  doubleClickTimer: null,
  liClick: function(e){
    if(e.target.tagName == "SVG") return 0
    if(e.target.tagName == "INPUT") return 0
    var dir = utils.domInLineage(e.target, {className: 'directoryItem'}).id
    if(e.target.className == 'name'){
      if(renderer.doubleClickLi){
        renderer.addRenameField(e.target)
        console.log(e.target)
        clearTimeout(renderer.doubleClickTimer)
        renderer.doubleClickLi = false
      }
      else {
        renderer.doubleClickLi = true
        renderer.doubleClickTimer = setTimeout(function(){
          // toggle showing the upload button if it's not already visible

          // Commented out as uploading functionality has been temporarily disabled
          // if (!renderer.selected) {
          //   renderer.showUpload(dir)
          // }
          renderer.viewDirectory(dir)
          renderer.doubleClickLi = false
        }, 200)
      }
    }
    else {
      // toggle showing the upload button if it's not already visible

      // Commented out as uploading functionality has been temporarily disabled
      // if (!renderer.selected) {
      //   renderer.showUpload(dir)
      // }
      renderer.viewDirectory(dir)
      renderer.doubleClickLi = false
    }
  },
  addRenameField: function(target){
    var input = document.createElement('input')
    input.type = 'text'
    input.value = target.innerHTML
    var previous = input.value

    target.parentElement.appendChild(input)
    input.focus()
    input.addEventListener('blur', renderer.renamePrototype)
    input.addEventListener('keydown', function(e){
      if (e.key == "Enter"){
        input.blur()
      }
      if(e.key == "Escape"){
        input.removeEventListener('blur', renderer.renamePrototype)
        renderer.removeRenameField(target.parentElement)
      }
      if(e.key == " "){
        target.parentElement.classList.add("warn")
      }
    })
    input.addEventListener("keyup", function(e){
      if(e.key == "Backspace" && !input.value.includes(" ")){
        target.parentElement.classList.remove("warn")
      }
    })
    target.parentElement.classList.add('focus')
    target.classList.add('hidden')
    target.parentElement.querySelector('.source-icon').classList.add('hidden')
    target.parentElement.querySelector('.world-icon').classList.add('hidden')

    // create icon to control inline message
    var warning = document.createElement("div")
    warning.classList.add("inlineIcon")

    renderer.setIconToShowInvalidProjectMessage(warning)

    target.parentElement.appendChild(warning)
  },
  removeRenameField: function(li){
    var input = li.querySelector('input')
    var name = li.querySelector('.name')
    name.classList.remove('hidden')
    li.querySelector('.source-icon').classList.remove('hidden')
    li.querySelector('.world-icon').classList.remove('hidden')
    li.removeChild(input)
    li.removeChild(li.querySelector(".inlineIcon"))
    li.classList.remove('focus')
  },
  renamePrototype: function(e){
    var newName = e.target.value
    if (!newName.includes(" ")){
      var newPath = path.join(PROTOTYPE_DIRECTORY, newName)
      var oldName = e.target.parentElement.querySelector('.name').innerHTML
      var oldPath = path.join(PROTOTYPE_DIRECTORY, oldName)
      if(newPath == oldPath) {
        renderer.removeRenameField(e.target.parentElement)
      } else {
        fs.rename(oldPath, newPath, function(err){
          if(err){
            renderer.log('{error}project rename failed')
            renderer.log('{error}' + err.message)
            return 0
          }
          renderer.log('{success}renamed ' + oldName + ' to ' + newName)
        })
      }
    }
    // the li will automatically be replaced due to the watcher on the PROTOTYPE_DIRECTORY
  },
  createDirLi: function(dir, index) {
    var li = document.createElement("li")
    li.className = "directoryItem"
    li.id = dir
    li.addEventListener("click", renderer.liClick)

    var name = document.createElement('div')
    name.classList.add('name')
    name.innerHTML = dir
    li.appendChild(name)

    // create svg icon
    var icon = document.createElementNS("http://www.w3.org/2000/svg", "svg")
    icon.classList.add("li-icon")
    icon.classList.add("source-icon")
    icon.setAttribute("proto-url", dir)
    icon.addEventListener("click", function(e) {
      renderer.openDirectory(this.getAttribute('proto-url'))
      e.stopPropagation()
    })

    var use = document.createElementNS("http://www.w3.org/2000/svg", "use");
    use.setAttributeNS("http://www.w3.org/1999/xlink", "href", "#icon-source");
    icon.appendChild(use)

    // create open browser svg
    var iconB = document.createElementNS("http://www.w3.org/2000/svg", "svg")
    iconB.classList.add("li-icon")
    iconB.classList.add("world-icon")
    iconB.setAttribute("proto-url", dir)
    iconB.addEventListener("click", function(e) {
      shell.openExternal("http://" + LOCAL_URL + "/" + this.getAttribute('proto-url'))
      e.stopPropagation()
    })

    var useB = document.createElementNS("http://www.w3.org/2000/svg", "use");
    useB.setAttributeNS("http://www.w3.org/1999/xlink", "href", "#icon-world");
    iconB.appendChild(useB)

    // append anchor and icon and insert into list
    li.appendChild(icon)
    li.appendChild(iconB)
    if (index > -1) {
      document.getElementById("directory").insertBefore(li, document.querySelector("#directory > li:nth-child(" + String(index + 1) + ")"))
    } else {
      document.getElementById("directory").appendChild(li)
    }
  },
  setIconToShowInvalidProjectMessage(icon) {
    icon.addEventListener("mouseenter", function(e) {
      if (icon.parentElement.classList.contains("warn")) {
        document.getElementById("invalidProjectName").classList.add("visible")
        document.getElementById("invalidProjectName").style.top = String(icon.getBoundingClientRect().top + 25) + "px"
      }
    })

    icon.addEventListener("mouseleave", function(e) {
      document.getElementById("invalidProjectName").classList.remove("visible")
    })
  },
  /* * * * * * * *
  *   UPLOADING  *
  * * * * * * * */
  // Commented out as uploading functionality has momentarily been removed
  // showUpload: function(dir) {
  //   document.getElementById('log').classList.toggle('short')
  //   document.getElementById('uploadContainer').classList.toggle('visible')
  // },
  // setUpload: function(dir) {
  //   document.getElementById('upload').addEventListener('click', function() {
  //     if (renderer.selected) {
  //       try {
  //         var options = {
  //           host: 'syd.jjcm.org',
  //           username:'k',
  //           path: PROTOTYPE_DIRECTORY + '/' + renderer.selected,
  //           proto: renderer.selected,
  //           privateKey: fs.readFileSync('privateKey_rsa')
  //         }
  //         if (document.getElementById('private').checked) {
  //           options.remoteDir = '/home/j/atl/' + renderer.selected
  //           options.type = 'private'
  //           renderer.log('{warn} Uploading ' + renderer.selected + ' to the private server <br>')
  //         } else {
  //           options.remoteDir = '/home/j/dev/' + renderer.selected
  //           options.type = 'public'
  //           renderer.log('{warn} Uploading ' + renderer.selected + ' to the public server <br>')
  //         }
  //         renderer.uploadDir(options)
  //       } catch (err) {
  //         renderer.log("{error}" + err)
  //       }
  //     }
  //   })
  // },
  // uploadDir: function(options) {
  //   sftp = new Sftp(options);
  //   sftp.on('error', function(err){
  //       renderer.log('{error}' + err);
  //   })
  //   .on('uploading', function(pgs){
  //       renderer.log('{warn} Uploading', pgs.file);
  //       renderer.log('{warn} ' + pgs.percent+'% completed');
  //   })
  //   .on('completed', function(){
  //       renderer.log('{success} Upload Completed');
  //       if (options.type == 'public') {
  //         renderer.log("{success} View your prototype at http://syd.jjcm.org/"+ options.proto)
  //         // document.getElementById('upload-link' + String(renderer.numUploads)).addEventListener('click', function() {
  //         //   shell.openExternal("http://syd.jjcm.org/" + options.proto)
  //         //   e.stopPropagation()
  //         // })enderer.uploadLink = "http://syd.jjcm.org/" + options.proto
  //       } else {
  //         renderer.log("{success} View your prototype at http://proto.office.atlassian.com/"+ options.proto)
  //         // renderer.uploadLink = "http://proto.office.atlassian.com/" + options.proto
  //       }
  //   }).upload()
  // },
  // clickUploadLink: function(){
  //   shell.openExternal(renderer.uploadlink)
  //   e.stopPropagation()
  // },
  /* * * * * * * *
  *   UPDATING   *
  * * * * * * * */
  toggleFlag: function(flag) {
    flag.classList.toggle('visible')
  },
  getNewVersion: function() {
    shell.openExternal("http://prototype.guide/server/prototypingServer.zip")
  },
}

document.addEventListener('DOMContentLoaded', renderer.init)
